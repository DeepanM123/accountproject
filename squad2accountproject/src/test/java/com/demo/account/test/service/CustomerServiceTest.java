
package com.demo.account.test.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import
org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import
org.mockito.Mockito;

import com.demo.account.dto.LoginDto;
import
com.demo.account.entity.Customer;
import com.demo.account.exception.ServiceException;
import
com.demo.account.repository.CustomerJpaRepository;
import
com.demo.account.service.CustomerService;

@RunWith(org.mockito.junit.MockitoJUnitRunner.Silent.class) public class
CustomerServiceTest {

	@Mock CustomerJpaRepository customerJpaRepository;

	@InjectMocks CustomerService customerService;

	List<Customer> ls = new ArrayList<Customer>();


	@Test(expected=Exception.class) 
	public void testLogIn() { 
		Customer customer =new Customer(); 
		customer.setAccountType("SAVING");
		customer.setAddress("address");

		ls.add(customer);
		LoginDto loginDto = new LoginDto();
		loginDto.setCustomerAccountNumber("123"); loginDto.setPassword("123");


		Mockito.when(customerJpaRepository.findByCustomerAccountNumberAndPassword(
				Mockito.anyString(),Mockito.anyString())).thenThrow(Exception.class);
		customerService.login(loginDto); Mockito.atLeastOnce();


	}


	/*
	 * @Test public void testLog1In2() throws ServiceException { List<Customer>
	 * customer=new ArrayList<Customer>(); LoginDto loginDto = new LoginDto();
	 * loginDto.setCustomerAccountNumber("1234"); loginDto.setPassword("latha");
	 * Mockito.when(((OngoingStubbing)
	 * customerJpaRepository.findByCustomerAccountNumberAndPassword("1234",
	 * "latha")).thenReturn(customer));
	 * 
	 * customerService.login(loginDto);
	 * 
	 * 
	 * 
	 * 
	 * }
	 */



	@Test(expected=ServiceException.class) 
	public void testLogInNegative() {

		Customer customer = new Customer();
		customer.setAccountType("SAVING");
		customer.setAddress("Address");
		customer.setCustomerAccountNumber("123");
		customer.setCustomerName("Alok");
		customer.setEmail("alok@gmail.com");
		customer.setIfsc("ICICI911");
		customer.setPassword("123");
		customer.setPhoneNumber("98977868");
		customer.setTotalSavings(1000.00);
		List<Customer> ls = new ArrayList<Customer>();
		LoginDto loginDto = new LoginDto(); 
//		ls.add(customer);
//		LoginDto loginDto = new LoginDto(); 
//		loginDto.setCustomerAccountNumber("1234");
//		loginDto.setPassword("latha");

		Mockito.when(customerJpaRepository.findByCustomerAccountNumberAndPassword(
				loginDto.getCustomerAccountNumber(),loginDto.getPassword())).thenReturn(ls);
		customerService.login(loginDto); Mockito.atLeastOnce();


	}


	@Before public void setUp() { 
		LoginDto loginDto = new LoginDto();
		loginDto.setCustomerAccountNumber("123"); 
		loginDto.setPassword("123"); }

	@After public void setup1() {

	}



	@Test 
	public void testLogIn2() {

		Customer customer = new Customer();
		customer.setAccountType("SAVING");
		customer.setAddress("Address");
		customer.setCustomerAccountNumber("123");
		customer.setCustomerName("Alok");
		customer.setEmail("alok@gmail.com");
		customer.setIfsc("ICICI911");
		customer.setPassword("123");
		customer.setPhoneNumber("98977868");
		customer.setTotalSavings(1000.00);
		List<Customer> ls = new ArrayList<Customer>();
		LoginDto loginDto = new LoginDto(); 
		loginDto.setCustomerAccountNumber("123");
		loginDto.setPassword("123");
		ls.add(customer);

		Mockito.when(customerJpaRepository.findByCustomerAccountNumberAndPassword(
				"123","123")).thenReturn(ls); 
		customerService.login(loginDto);




	}



}
