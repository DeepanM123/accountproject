package com.demo.account.test.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.demo.account.controller.BeneficiaryController;
import com.demo.account.dto.BeneficiaryDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.Beneficiary;
import com.demo.account.exception.ServiceException;
import com.demo.account.repository.BeneficiaryRepository;
import com.demo.account.repository.CustomerJpaRepository;
import com.demo.account.serviceImpl.BeneficiaryServiceImpl;

 

@RunWith(org.mockito.junit.MockitoJUnitRunner.Silent.class)
public class BeneficiaryTest {
    
    Beneficiary beneficiary;
    
    @Mock
    BeneficiaryController beneficiarycontroller;
    @Mock
    BeneficiaryRepository beneficiaryrepository;
    @Mock
    CustomerJpaRepository customerJpaRepository;
    
    @InjectMocks
    BeneficiaryServiceImpl beneficiaryServiceImpl;
   
    
    
    

@Before
public void setUp() {
	
}
 

    @After
    public void setup1() {
 
    }
    
   
        
        
        @Test
        public void addBeneficiaryTestData() {
        	List<Beneficiary> list= new ArrayList<Beneficiary>();
        	Beneficiary b = new Beneficiary();
        	
        	b.setBeneficiaryAccountNumber("321");
        	b.setBeneficiaryName("Alok");
        	b.setCustomerAccountNumber("123");
        	list.add(b);
        	BeneficiaryDto dao = new BeneficiaryDto();
        	dao.setCustomerAccountNumber("123");
        	dao.setListBeneficiary(list);
        	Mockito.when(customerJpaRepository.checkWhetherCustomerIsPresent(Mockito.anyString())).thenReturn(2);
        	Mockito.when(beneficiaryrepository.fetchAllData()).thenReturn(list);
        	 beneficiaryServiceImpl.saveBeneficiaryData(dao);
        	
        }
        
        
        
        @Test(expected=ServiceException.class)
        public void addBeneficiaryTestData2() {
        	List<Beneficiary> list= new ArrayList<Beneficiary>();
        	Beneficiary b = new Beneficiary();
        	
        	b.setBeneficiaryAccountNumber("321");
        	b.setBeneficiaryName("Alok");
        	b.setCustomerAccountNumber("123");
        	list.add(b);
        	BeneficiaryDto dao = new BeneficiaryDto();
        	dao.setCustomerAccountNumber("123");
        	dao.setListBeneficiary(list);
        	Mockito.when(beneficiaryrepository.fetchAllData()).thenReturn(list);
        	
        	TransactionResponse expected = beneficiaryServiceImpl.saveBeneficiaryData(dao);
        	assertEquals(b,expected);
        }
        
        
        

        
        @Test(expected=ServiceException.class)
        public void testFetchBeneficiaryDataNegative() {
        	List<Beneficiary> ls = new ArrayList<Beneficiary>();
        	Beneficiary b = new Beneficiary();
        	b.setBeneficiaryAccountNumber("321");
        	b.setCustomerAccountNumber("123");
        	b.setBeneficiaryName("Alok");
        	Mockito.when(beneficiaryrepository.getDataByCustomerAccountAndBeneficiary(b.getCustomerAccountNumber(), b.getBeneficiaryAccountNumber())).thenReturn(100);
        	Mockito.when(beneficiaryrepository.getDetails(b.getCustomerAccountNumber())).thenReturn(ls);
        	beneficiaryServiceImpl.fetchBeneficiaryData("1234567");
        }
        
    
}
