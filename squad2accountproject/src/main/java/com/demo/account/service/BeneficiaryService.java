package com.demo.account.service;

import java.util.List;

import com.demo.account.dto.BeneficiaryDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.Beneficiary;

public interface BeneficiaryService {

	public TransactionResponse saveBeneficiaryData(BeneficiaryDto beneficiaryDto);

	public List<Beneficiary> fetchBeneficiaryData(String customerAccountNumber) ;

}
