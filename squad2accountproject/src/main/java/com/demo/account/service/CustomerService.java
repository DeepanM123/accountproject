package com.demo.account.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.account.dto.LoginDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.Customer;
import com.demo.account.exception.ErrorConstant;
import com.demo.account.exception.ServiceException;
import com.demo.account.repository.CustomerJpaRepository;

@Service
public class CustomerService {

 

    @Autowired
    CustomerJpaRepository customerRepository;

 

    public TransactionResponse login(LoginDto loginDto) {
        List<Customer> customerList = customerRepository
                .findByCustomerAccountNumberAndPassword(loginDto.getCustomerAccountNumber(), loginDto.getPassword());

        if (customerList.isEmpty()) {
        	throw new ServiceException(ErrorConstant.NO_LOGIN_DETAILS_FOUND.getMessage(),ErrorConstant.NO_LOGIN_DETAILS_FOUND.getId());

        } else {
        	return new TransactionResponse("Log In is successfull",200);
        }
    }
}
