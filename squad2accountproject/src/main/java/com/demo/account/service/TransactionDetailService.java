package com.demo.account.service;

import java.util.List;

import javax.validation.Valid;

import com.demo.account.dto.TransactionDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.TransactionSummary;

public interface TransactionDetailService {

	
	public TransactionResponse saveTransactionData( @Valid TransactionDto transactionSummary);

	public List<TransactionSummary> fetchTransactionDataOnBasisOfCustomerAccountNumber(String customerAccountNumber);
	
	

}
