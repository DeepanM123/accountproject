package com.demo.account.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.account.dto.TransactionDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.TransactionSummary;
import com.demo.account.service.TransactionDetailService;

@RestController
public class TransactionDetailController {
	
	@Autowired
	TransactionDetailService transactionDetailService;
	
/**
     
* As part of this API we are saving transaction details.
     
* @throws insufficient balance exception if the transfer amount is more than amount in the customer account number
     
* @author Alok
     
*
     
* @param transactionSummary
     
* @return
     
*/
	@PostMapping("/transactions")
	public ResponseEntity<TransactionResponse> saveTransactionDetailsInformation(@Valid @RequestBody TransactionDto transactionSummary){
	return new  ResponseEntity<>(transactionDetailService.saveTransactionData(transactionSummary),HttpStatus.OK);
	}


/**
     
* As part of this API we are getting transaction details on the basis of customer account number
     
* @author Alok
     
* @param customerAccountNumber
     
* @return
     
*/
	@GetMapping("/transactionDetails/{customerAccountNumber}")
	public ResponseEntity<List<TransactionSummary>> fetchTransactionDataOnBasisOfCustomerAccountNumber(@PathVariable("customerAccountNumber") String customerAccountNumber){
		 return new ResponseEntity<>(transactionDetailService.fetchTransactionDataOnBasisOfCustomerAccountNumber(customerAccountNumber),HttpStatus.OK);
	}
	
	

}
