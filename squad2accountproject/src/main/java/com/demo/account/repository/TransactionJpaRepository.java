package com.demo.account.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.account.entity.TransactionSummary;

@Repository
public interface TransactionJpaRepository extends JpaRepository<TransactionSummary, Long>{

	@Query("Select entity from TransactionSummary entity where entity.fromAccount =?1")
	public List<TransactionSummary> fetchData(String customerAccountNumber);

}
