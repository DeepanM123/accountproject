package com.demo.account.serviceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.account.dto.TransactionDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.TransactionSummary;
import com.demo.account.exception.ErrorConstant;
import com.demo.account.exception.ServiceException;
import com.demo.account.repository.BeneficiaryRepository;
import com.demo.account.repository.CustomerJpaRepository;
import com.demo.account.repository.TransactionJpaRepository;
import com.demo.account.service.TransactionDetailService;


/**
 * 
 * @author ALok
 * This service will save the transaction data
 * @Exception thows exception when balance is insufficient
 * and when customer and benificiary is not added
 *
 */
@Service
public class TransactionDetailServiceImpl implements TransactionDetailService{

	@Autowired
	TransactionJpaRepository transactionJpaRepository;

	@Autowired
	BeneficiaryRepository beneficiaryRespository;

	@Autowired
	CustomerJpaRepository customerJpaRepository;


	@Override
	public synchronized TransactionResponse  saveTransactionData(@Valid TransactionDto transactionDto) {



		int count= beneficiaryRespository.checkFromAccountAndToAccount(transactionDto.getFromAccount(), transactionDto.getToAccount());
		double amount =0;
		if(count >0) {
			amount = customerJpaRepository.getTotalSavingsOfCustomer(transactionDto.getFromAccount());
		}else {
			throw new ServiceException(ErrorConstant.NO_DATA_WITH_CUSTOMER_AND_BENFECIARY.getMessage(),ErrorConstant.NO_DATA_WITH_CUSTOMER_AND_BENFECIARY.getId());
		}
		if(amount < transactionDto.getAmountTransferred()) { 
			throw new ServiceException(ErrorConstant.INSUFFICIENT_BALANCE.getMessage(),ErrorConstant.INSUFFICIENT_BALANCE.getId());
		}else {

			TransactionSummary transactionSummary=new TransactionSummary();
			BeanUtils.copyProperties(transactionDto, transactionSummary);
			LocalDate date = LocalDate.now();
			
			double substractedAmount = amount- transactionSummary.getAmountTransferred();

			customerJpaRepository.updateAmountOfCustomer(substractedAmount,transactionSummary.getFromAccount());
			transactionSummary.setTransactionDate(date.toString());

			
			
			transactionJpaRepository.save(transactionSummary);
			return new TransactionResponse("Transaction Data Successfully Saved",200);
		}


	}


	@Override
	public List<TransactionSummary> fetchTransactionDataOnBasisOfCustomerAccountNumber(String customerAccountNumber) {
		List<TransactionSummary> ls = new ArrayList<>();
		ls = transactionJpaRepository.fetchData(customerAccountNumber);
		if(!ls.isEmpty()) {
			return ls;
		}else {
			throw new ServiceException(ErrorConstant.NO_TRANSACTION_FOUND_WITH_CUSTOMER_ACCOUNT_NUMBER.getMessage(),ErrorConstant.NO_TRANSACTION_FOUND_WITH_CUSTOMER_ACCOUNT_NUMBER.getId());
		}
	}

}


