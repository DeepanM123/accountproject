package com.demo.account.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.account.dto.BeneficiaryDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.Beneficiary;
import com.demo.account.exception.ErrorConstant;
import com.demo.account.exception.ServiceException;
import com.demo.account.repository.BeneficiaryRepository;
import com.demo.account.repository.CustomerJpaRepository;
import com.demo.account.service.BeneficiaryService;

@Service
public class BeneficiaryServiceImpl implements BeneficiaryService{

	@Autowired
	BeneficiaryRepository beneficiaryRepository;

	@Autowired
	CustomerJpaRepository customerJpaRepository;

	int total =0;
	@Override
	public TransactionResponse saveBeneficiaryData(BeneficiaryDto beneficiaryDto) {

		int count = customerJpaRepository.checkWhetherCustomerIsPresent(beneficiaryDto.getCustomerAccountNumber());
		
				
	
		if(count > 0) {
			beneficiaryDto.getListBeneficiary().forEach(customer->{
				customer.setCustomerAccountNumber(beneficiaryDto.getCustomerAccountNumber());
				int count2 = customerJpaRepository.checkWhetherCustomerIsPresent(customer.getBeneficiaryAccountNumber());
				if(count2 > 0) {
				List<Beneficiary> list= new ArrayList<>();
				list =beneficiaryRepository.fetchAllData();
				list.forEach(item->{
					total =beneficiaryRepository.getDataByCustomerAccountAndBeneficiary(customer.getCustomerAccountNumber(),customer.getBeneficiaryAccountNumber());
				});
				if(total >0) {
					throw new ServiceException("Benefeciary already added by customer",106);
				}
				}else {
					throw new ServiceException("Given Beneficiary Account Number is not a valid Account number",107);
				}
				beneficiaryRepository.save(customer);

			});
		}else {
			throw new  ServiceException(ErrorConstant.REGISTER.getMessage(),ErrorConstant.REGISTER.getId());
		}
		return new TransactionResponse("Beneficiary Data Successfully Saved",200);



	}

	@Override
	public List<Beneficiary> fetchBeneficiaryData(String customerAccountNumber)  {
		List<Beneficiary> lf = null;
		lf = beneficiaryRepository.getDetails(customerAccountNumber);
		if(!lf.isEmpty()) {
			return lf;
		}else {
			throw new ServiceException(ErrorConstant.NO_DATA_FOUND_WITH_CUSTOMER_ACCOUNT_NUMBER.getMessage(),ErrorConstant.NO_DATA_FOUND_WITH_CUSTOMER_ACCOUNT_NUMBER.getId());
		}
	}
}