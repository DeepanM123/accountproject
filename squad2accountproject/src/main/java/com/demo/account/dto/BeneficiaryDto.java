package com.demo.account.dto;

import java.util.List;

import com.demo.account.entity.Beneficiary;

public class BeneficiaryDto {
	
	private String customerAccountNumber;
	
	private List<Beneficiary> listBeneficiary;

	public String getCustomerAccountNumber() {
		return customerAccountNumber;
	}

	public void setCustomerAccountNumber(String customerAccountNumber) {
		this.customerAccountNumber = customerAccountNumber;
	}

	public List<Beneficiary> getListBeneficiary() {
		return listBeneficiary;
	}

	public void setListBeneficiary(List<Beneficiary> listBeneficiary) {
		this.listBeneficiary = listBeneficiary;
	}
	
	
	
	
	
	

}
