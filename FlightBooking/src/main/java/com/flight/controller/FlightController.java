package com.flight.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flight.airlineservice.AirLineService;
import com.flight.airlineservice.FlightService;
import com.flight.airlineservice.RouteService;
import com.flight.dto.FlightScheduleRequest;
import com.flight.dto.FlightScheduleResponse;
import com.flight.dto.RouteResponse;
import com.flight.entity.Flight;
import com.flight.exceptions.AirLineNotFoundException;
import com.flight.exceptions.FlightNotFoundException;
import com.flight.exceptions.SourceOrDestinationAirportNotFound;

@RestController
public class FlightController {
	@Autowired
	AirLineService airLineService;
	@Autowired
	FlightService flightService;
	@Autowired
	RouteService routeService;
	
	@GetMapping("airlines/{airlinename}")
	public List<Flight> getflights(@PathVariable(value = "airlinename") String airlinename) throws AirLineNotFoundException
	{
		return airLineService.getallflights(airlinename);
	}
	
	@PostMapping("flights")
	public List<FlightScheduleResponse> findcurrentscheduledflight(@RequestBody FlightScheduleRequest flightScheduleRequest) throws FlightNotFoundException
	{
		return flightService.findcurrentscheduledflights(flightScheduleRequest);
	}
	
	@GetMapping("routes")
	public List<RouteResponse> getflightsfromroute(@RequestParam String source,@RequestParam String destination,@RequestParam String date)throws FlightNotFoundException,SourceOrDestinationAirportNotFound
	{
		return routeService.getflightsfromroutes(source,destination,date);
	}
}
