package com.flight.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flight.entity.Flight;

public interface FlightRepository extends JpaRepository<Flight, Integer>{

	List<Flight> findAllByAirlines(String airlinename);

	public Flight findByFlightid(int flightid);

}
