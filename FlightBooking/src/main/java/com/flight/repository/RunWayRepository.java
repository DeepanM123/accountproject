package com.flight.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flight.entity.RunWay;

public interface RunWayRepository extends JpaRepository<RunWay, Integer>{

}
