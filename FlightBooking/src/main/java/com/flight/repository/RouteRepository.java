package com.flight.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flight.entity.Route;

public interface RouteRepository extends JpaRepository<Route, Integer>{

	List<Route> findAllBySourceairportidAndDestinationaiportid(int airportid, int airportid2);

}
