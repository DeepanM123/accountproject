package com.flight.exceptions;

public class FlightNotFoundException extends Exception {

	private static final long serialVersionUID = -9079454849611061074L;

	public FlightNotFoundException() {
		super();
	}

	public FlightNotFoundException(final String message) {
		super(message);
	}

}
