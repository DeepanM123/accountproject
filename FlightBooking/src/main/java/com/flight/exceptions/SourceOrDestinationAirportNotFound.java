package com.flight.exceptions;


public class SourceOrDestinationAirportNotFound extends Exception {

	private static final long serialVersionUID = -9079454849611066574L;

	public SourceOrDestinationAirportNotFound() {
		super();
	}

	public SourceOrDestinationAirportNotFound(final String message) {
		super(message);
	}
}