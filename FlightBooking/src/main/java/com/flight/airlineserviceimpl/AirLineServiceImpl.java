package com.flight.airlineserviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flight.airlineservice.AirLineService;
import com.flight.entity.Flight;
import com.flight.exceptions.AirLineNotFoundException;
import com.flight.repository.FlightRepository;
@Service
public class AirLineServiceImpl implements AirLineService{
	
	@Autowired
	FlightRepository flightRepository;
	
	public List<Flight> getallflights(String airlinename) throws AirLineNotFoundException
	{
		List<Flight> flight=flightRepository.findAllByAirlines(airlinename);
		
		if(flight.isEmpty())
		{
			throw new AirLineNotFoundException("No Airlines available");
		}
		else
		{
			return flight;
		}
	}

}


