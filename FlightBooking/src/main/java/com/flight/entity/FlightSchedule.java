package com.flight.entity;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.flight.enums.LandType;

@Entity
public class FlightSchedule {
	@Id
	@GeneratedValue
	private int scheduleid;
	
	private int flightid;
	private int airportid;
	private LocalDateTime datetime;
	@Enumerated(EnumType.STRING)
	private LandType landtype;
	public int getScheduleid() {
		return scheduleid;
	}
	public void setScheduleid(int scheduleid) {
		this.scheduleid = scheduleid;
	}
	public int getFlightid() {
		return flightid;
	}
	public void setFlightid(int flightid) {
		this.flightid = flightid;
	}
	public int getAirportid() {
		return airportid;
	}
	public void setAirportid(int airportid) {
		this.airportid = airportid;
	}
	public LocalDateTime getdatetime() {
		return datetime;
	}
	public void setdatetime(LocalDateTime datetime) {
		this.datetime = datetime;
	}
	public LandType getLandtype() {
		return landtype;
	}
	public void setLandtype(LandType landtype) {
		this.landtype = landtype;
	}
	public FlightSchedule(int scheduleid, int flightid, int airportid, LocalDateTime datetime, LandType landtype) {
		super();
		this.scheduleid = scheduleid;
		this.flightid = flightid;
		this.airportid = airportid;
		this.datetime = datetime;
		this.landtype = landtype;
	}
	public FlightSchedule() {
		super();

	}
	
	
}
