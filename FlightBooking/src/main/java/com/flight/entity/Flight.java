package com.flight.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Flight {

	@Id
	@GeneratedValue
	private int flightid;
	private String airlines;
	private int capacity;
	private String servicetype;

	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public int getFlightid() {
		return flightid;
	}

	public void setFlightid(int flightid) {
		this.flightid = flightid;
	}

	public String getAirlines() {
		return airlines;
	}

	public void setAirlines(String airlines) {
		this.airlines = airlines;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Flight(int flightid, String airlines, int capacity,String servicetype) {
		super();
		this.flightid = flightid;
		this.airlines = airlines;
		this.capacity = capacity;
		this.servicetype=servicetype;
	}

	public Flight() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy = "flight")

	private Set<Route> route;

	public Flight(Set<Route> route) {
		super();
		this.route = route;
	}
	@JsonIgnore
	public Set<Route> getRoute() {
		return route;
	}

	public void setRoute(Set<Route> route) {
		this.route = route;
	}
	

}
