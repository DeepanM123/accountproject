package com.flight.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class AirLine {
	
	@Id
	@GeneratedValue
	private int airlineid;
	private String airlinename;
	int getAirlineid() {
		return airlineid;
	}
	void setAirlineid(int airlineid) {
		this.airlineid = airlineid;
	}
	String getAirlinename() {
		return airlinename;
	}
	void setAirlinename(String airlinename) {
		this.airlinename = airlinename;
	}
	public AirLine(int airlineid, String airlinename) {
		super();
		this.airlineid = airlineid;
		this.airlinename = airlinename;
	}
	public AirLine() {
		super();
		
	}
	
	

}
