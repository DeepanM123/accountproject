package com.flight.dto;

import java.time.LocalDate;

public class FlightScheduleRequest {
	
	private int flightid;
	private int airportid;
	LocalDate date;
	public int getFlightid() {
		return flightid;
	}
	public void setFlightid(int flightid) {
		this.flightid = flightid;
	}
	public int getAirportid() {
		return airportid;
	}
	public void setAirportid(int airportid) {
		this.airportid = airportid;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public FlightScheduleRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FlightScheduleRequest(int flightid, int airportid, LocalDate date) {
		super();
		this.flightid = flightid;
		this.airportid = airportid;
		this.date = date;
	}
	

}
