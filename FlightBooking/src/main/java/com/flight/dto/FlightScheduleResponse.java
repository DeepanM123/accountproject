package com.flight.dto;

import java.time.LocalDate;

public class FlightScheduleResponse {
	
	private int flightid;
	private int airportid;
	private LocalDate date;
	private String landtype;
	private String airline;
	public int getFlightid() {
		return flightid;
	}
	public void setFlightid(int flightid) {
		this.flightid = flightid;
	}
	public int getAirportid() {
		return airportid;
	}
	public void setAirportid(int airportid) {
		this.airportid = airportid;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getLandtype() {
		return landtype;
	}
	public void setLandtype(String landtype) {
		this.landtype = landtype;
	}
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public FlightScheduleResponse() {
		super();

	}
	public FlightScheduleResponse(int flightid, int airportid, LocalDate date, String landtype, String airline) {
		super();
		this.flightid = flightid;
		this.airportid = airportid;
		this.date = date;
		this.landtype = landtype;
		this.airline = airline;
	}
	

}
