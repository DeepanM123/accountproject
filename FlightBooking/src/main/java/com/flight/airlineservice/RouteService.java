package com.flight.airlineservice;

import java.util.List;

import org.springframework.stereotype.Service;
import com.flight.dto.RouteResponse;
import com.flight.exceptions.FlightNotFoundException;
import com.flight.exceptions.SourceOrDestinationAirportNotFound;

@Service
public interface RouteService {
	
	public List<RouteResponse> getflightsfromroutes(String source,String destination,String date) throws FlightNotFoundException,SourceOrDestinationAirportNotFound;

}
