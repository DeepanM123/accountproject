package com.flight.Controller;



import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import com.flight.airlineservice.AirLineService;
import com.flight.airlineservice.FlightService;
import com.flight.airlineservice.RouteService;
import com.flight.controller.FlightController;
import com.flight.dto.FlightScheduleRequest;
import com.flight.dto.FlightScheduleResponse;
import com.flight.dto.RouteResponse;
import com.flight.entity.Flight;
import com.flight.exceptions.AirLineNotFoundException;
import com.flight.exceptions.FlightNotFoundException;
import com.flight.exceptions.SourceOrDestinationAirportNotFound;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FlightControllerTests {

	@Mock
	AirLineService airlineService;
	
	@Mock
	FlightService flightService;
	
	@Mock
	RouteService routeService;
	
	@InjectMocks
	FlightController flightController;
	String airlinename;
	String source;
	String destination;
	String date;

	Flight flight = new Flight();
	FlightScheduleResponse flightScheduleResponse;
	FlightScheduleRequest flightScheduleRequest;
	RouteResponse routeResponse;
	List<Flight> listOfFlights;
	List<FlightScheduleResponse> listOfFlightScheduleResponse;
	List<RouteResponse> listOfRouteResponse;
	
	@Before
	public void setup() {
		flight.setAirlines("spc");
		flight.setServicetype("air");
		flightScheduleResponse=new FlightScheduleResponse();
		flightScheduleRequest=new FlightScheduleRequest();
		routeResponse=new RouteResponse();
		flightScheduleResponse.setAirline("spc");
		flightScheduleResponse.setLandtype("takeoff");
		flightScheduleResponse.setAirportid(1);
		flightScheduleResponse.setFlightid(1);
		flight.setCapacity(4);
		airlinename = "JETAirways";
		source="deepanhome";
		destination="kusumahome";
		date="2222-22-22";
		listOfFlights = new ArrayList<>();
		listOfFlights.add(flight);
		listOfFlightScheduleResponse=new ArrayList<>();
		listOfFlightScheduleResponse.add(flightScheduleResponse);
		listOfRouteResponse=new ArrayList<>();
		routeResponse.setAvailableseats(6);
		routeResponse.setDestination("kusumahome");
		routeResponse.setSource("deepanhome");
		listOfRouteResponse.add(routeResponse);
	}

	@Test
	public void getAirlines() throws AirLineNotFoundException {

		Mockito.when(airlineService.getallflights(Mockito.anyString())).thenReturn(listOfFlights);
		List<Flight> actualValue = flightController.getflights(airlinename);
		Assert.assertEquals(1, actualValue.size());

	}
	@Test
	public void getSchecdule() throws FlightNotFoundException
	{
		Mockito.when(flightService.findcurrentscheduledflights(Mockito.any(FlightScheduleRequest.class))).thenReturn(listOfFlightScheduleResponse);
		List<FlightScheduleResponse> actual=flightController.findcurrentscheduledflight(flightScheduleRequest);
		Assert.assertEquals(1, actual.size());
	}
	
	@Test
	public void getFlightsFromRoute() throws FlightNotFoundException, SourceOrDestinationAirportNotFound 
	{
		Mockito.when(routeService.getflightsfromroutes(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(listOfRouteResponse);
		List<RouteResponse> actual=flightController.getflightsfromroute(source, destination, date);
		Assert.assertEquals(1, actual.size());
	}
}