package com.demo.account.exception;


	
	public class ServiceException extends RuntimeException{
		/** The Constant serialVersionUID. */

		private static final long serialVersionUID = 1L;


		/** The message. */
		private final String message;

		/** The error code. */
		private final int errorCode;

		

		public ServiceException(String message, int errorCode) {
			super();
			this.message = message;
			this.errorCode = errorCode;
		}

		public String getMessage() {
			return message;
		}

		public int getErrorCode() {
			return errorCode;
		}

		
		
}
