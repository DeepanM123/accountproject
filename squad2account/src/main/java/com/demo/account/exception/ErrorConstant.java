package com.demo.account.exception;

public enum ErrorConstant {
	NO_DATA_FOUND_WITH_CUSTOMER_ACCOUNT_NUMBER(101,"No data found with Customer Account number"), 
	NO_DATA_WITH_CUSTOMER_AND_BENFECIARY( 102,"No data found with Customer And Beneficiery"), 
	NO_TRANSACTION_FOUND_WITH_CUSTOMER_ACCOUNT_NUMBER(103,"No Transaction found with Customer Account number"), 
	INSUFFICIENT_BALANCE(104,"Insufficient Balance to Transfer"), 
	NO_LOGIN_DETAILS_FOUND(105,"No Login Detail Found"), 
	REGISTER(106,"Please register First");
	

	


	
	private int id;
	private final String message;
	public int getId() {
		return id;
	}
	 void setId(int id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	private ErrorConstant(int id, String message) {
		this.id = id;
		this.message = message;
	}


	
	
}
