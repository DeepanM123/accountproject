package com.demo.account.exception;

public class AppConstant {
	
	 public static final String BENEFICIARY_NOT_FOUND="record not found";
	    public static final String NO_RECORD_FOUND="no record found";
	    public static final String ALREADY_EXISTED="record already  existed";
	    public static final String CUSTOMER_LOGIN_ERROR = "invalid username or password";
	    public static final String CUSTOMER_LOGIN_SUCCESS = "logged in successfully";

}
