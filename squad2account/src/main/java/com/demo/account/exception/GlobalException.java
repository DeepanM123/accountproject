package com.demo.account.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalException extends ResponseEntityExceptionHandler {
	@ExceptionHandler(ServiceException.class)
	ResponseEntity<ErrorStatus> showresponse(ServiceException serviceException){
		ErrorStatus er= new ErrorStatus();
		er.setErrorCode(serviceException.getErrorCode());
		er.setErrorMessage(serviceException.getMessage());
		return new ResponseEntity<>(er,HttpStatus.NOT_FOUND);
		
	}
	


}
