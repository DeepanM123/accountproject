package com.demo.account.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CUSTOMER_DETAIL_TB")
public class Customer {


	 

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private long id;
	    private String customerName;
	    private String customerAccountNumber;
	    private String address;
	    private String phoneNumber;
	    private String email;
	    private String ifsc;
	    private String accountType;
	    private String password;
	     private double totalSavings;
	    public long getId() {
	        return id;
	    }
	    public void setId(long id) {
	        this.id = id;
	    }
	    public String getCustomerName() {
	        return customerName;
	    }
	    public void setCustomerName(String customerName) {
	        this.customerName = customerName;
	    }
	    public String getCustomerAccountNumber() {
	        return customerAccountNumber;
	    }
	    public void setCustomerAccountNumber(String customerAccountNumber) {
	        this.customerAccountNumber = customerAccountNumber;
	    }
	    public String getAddress() {
	        return address;
	    }
	    public void setAddress(String address) {
	        this.address = address;
	    }
	    public String getPhoneNumber() {
	        return phoneNumber;
	    }
	    public void setPhoneNumber(String phoneNumber) {
	        this.phoneNumber = phoneNumber;
	    }
	    public String getEmail() {
	        return email;
	    }
	    public void setEmail(String email) {
	        this.email = email;
	    }
	    public String getIfsc() {
	        return ifsc;
	    }
	    public void setIfsc(String ifsc) {
	        this.ifsc = ifsc;
	    }
	    public String getAccountType() {
	        return accountType;
	    }
	    public void setAccountType(String accountType) {
	        this.accountType = accountType;
	    }
	    public String getPassword() {
	        return password;
	    }
	    public void setPassword(String password) {
	        this.password = password;
	    }
	    public double getTotalSavings() {
	        return totalSavings;
	    }
	    public void setTotalSavings(double totalSavings) {
	        this.totalSavings = totalSavings;
	    }
	}
