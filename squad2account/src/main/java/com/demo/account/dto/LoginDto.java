package com.demo.account.dto;

public class LoginDto {

	 

    private String customerAccountNumber;
    private String password;

 

    public String getCustomerAccountNumber() {
        return customerAccountNumber;
    }

 

    public void setCustomerAccountNumber(String customerAccountNumber) {
        this.customerAccountNumber = customerAccountNumber;
    }

 

    public String getPassword() {
        return password;
    }

 

    public void setPassword(String password) {
        this.password = password;
    }
}