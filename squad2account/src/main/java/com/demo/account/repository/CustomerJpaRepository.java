package com.demo.account.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.account.entity.Customer;

/**
 * 
 * @author Alok
 * This is a Customer JPA Repository
 *
 */




@Transactional
@Repository
public interface CustomerJpaRepository extends JpaRepository<Customer, Long>{
	
	
	@Query("Select entity.totalSavings from Customer entity where entity.customerAccountNumber =?1")
	public double getTotalSavingsOfCustomer(String customerAccountNumber);

	@Modifying
	@Query("UPDATE Customer entity SET entity.totalSavings = ?1 WHERE entity.customerAccountNumber =?2")
	public void updateAmountOfCustomer(double substractedAmount, String fromAccount);
	
	List<Customer> findByCustomerAccountNumberAndPassword(String customerAccountNumber, String password);

	 

	Optional<List<Customer>> findByCustomerAccountNumber(String customerAccountNumber);

	@Query("Select count(entity) from Customer entity where entity.customerAccountNumber =?1")
	public int checkWhetherCustomerIsPresent(String customerAccountNumber);

}
