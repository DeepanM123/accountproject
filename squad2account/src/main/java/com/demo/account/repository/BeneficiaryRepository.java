package com.demo.account.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.account.entity.Beneficiary;

@Repository
public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Long>{
	
		  
	
		public Optional<Beneficiary> findByCustomerAccountNumber(String customeraccountnumber);  	
		public Optional<Beneficiary> findByBeneficiaryName(String beneficiaryName);
		
		@Query("Select count(entity) from Beneficiary entity where entity.customerAccountNumber =?1 And entity.beneficiaryAccountNumber=?2")
		public int checkFromAccountAndToAccount(String fromAccount, String toAccount);
		
		@Query("Select entity from Beneficiary entity where entity.customerAccountNumber =?1")
		public List<Beneficiary> getDetails(String customerAccountNumber);
		
		@Query("Select count(entity) from Beneficiary entity where entity.customerAccountNumber =?1 AND entity.beneficiaryAccountNumber =?2")
		public int getDataByCustomerAccountAndBeneficiary(String customerAccountNumber, String beneficiaryAccountNumber);
		
		@Query("Select entity from Beneficiary entity")
		public List<Beneficiary> fetchAllData();
		
		
		  }

