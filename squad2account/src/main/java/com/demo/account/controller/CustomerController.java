package com.demo.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.account.dto.LoginDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.service.CustomerService;

@RestController
public class CustomerController {

 

    @Autowired
    CustomerService customerService;

 

    /**
     
* If User want to make a transaction he must have to login. For that here we
     
* are going to provide logIn method. and if any error occurs it shows
     
* exception. for that exception handing is created.
     
* @author Latha V
     
*
     
* @param loginDto
     
* @return
     
*
     
*/


 

    @PostMapping("/login")
    public ResponseEntity<TransactionResponse> loginCustomer(@RequestBody LoginDto loginDto) {
        return new ResponseEntity<>(customerService.login(loginDto), HttpStatus.OK);
    }
}
