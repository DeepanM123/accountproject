package com.demo.account.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.account.dto.BeneficiaryDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.Beneficiary;
import com.demo.account.repository.BeneficiaryRepository;
import com.demo.account.service.BeneficiaryService;

@RestController
public class BeneficiaryController {
	
	@Autowired
	BeneficiaryRepository beneficiaryRepository;
	
	@Autowired
	BeneficiaryService beneficiaryService;
	
/**
    
* As part of this API customer can add beneficiary details
     
* @throws Exception if beneficiary account is not a valid account
     
* @throws Exception if beneficiary is added by the customer
    
* @author Deepan M
     
*
     
* @param beneficiaryDto
     
* @return
     
*/
	@PostMapping("/beneficiary")
	public  ResponseEntity<TransactionResponse> addBeneficiary(@RequestBody BeneficiaryDto beneficiaryDto) {
		return new  ResponseEntity<>(beneficiaryService.saveBeneficiaryData(beneficiaryDto),HttpStatus.OK);
		
	}

	
/**
     
* As part of this API we are getting beneficiary details on the basis of customer account number
     
* @author Deepan M
     
* @param customerAccountNumber
     
* @return
     
*/
	@GetMapping("/beneficiary/{customerAccountNumber}")
	public  ResponseEntity<List<Beneficiary>> getBeneficiaryDetails(@PathVariable("customerAccountNumber") String customerAccountNumber){
		return new ResponseEntity<List<Beneficiary>>(beneficiaryService.fetchBeneficiaryData(customerAccountNumber),HttpStatus.OK);
	}
	
}
