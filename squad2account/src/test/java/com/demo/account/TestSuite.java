package com.demo.account;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.demo.account.test.controller.BeneficieryControllerTest;
import com.demo.account.test.controller.CustomerControllerTest;
import com.demo.account.test.controller.TransactionDetailControllerTest;
import com.demo.account.test.service.BeneficiaryTest;
import com.demo.account.test.service.CustomerServiceTest;
import com.demo.account.test.service.SavingAccountServiceImplTest;

@RunWith(Suite.class)
@SuiteClasses({BeneficiaryTest.class, SavingAccountServiceImplTest.class,CustomerServiceTest.class ,BeneficieryControllerTest.class,  TransactionDetailControllerTest.class, CustomerControllerTest.class })
public class TestSuite {

}
