package com.demo.account.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.demo.account.controller.TransactionDetailController;
import com.demo.account.dto.TransactionDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.TransactionSummary;
import com.demo.account.service.TransactionDetailService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TransactionDetailControllerTest {

	@Mock
	TransactionDetailService transactionDetailService;

	@InjectMocks
	TransactionDetailController transactionDetailController;




	TransactionDto transactionDto = new TransactionDto();




	TransactionResponse response = new TransactionResponse("Transaction Successfully", 200);





	@Before
	public void setup() {
		transactionDto.setAmountTransferred(1000);
		transactionDto.setFromAccount("123");
		transactionDto.setToAccount("321");
	}    



	@Test 
	public void saveTransactionTest() {
		when(transactionDetailService.saveTransactionData(transactionDto)).thenReturn(response);
		ResponseEntity<TransactionResponse> expected = transactionDetailController.saveTransactionDetailsInformation(transactionDto);        
		assertEquals(HttpStatus.OK, expected.getStatusCode());
		Mockito.verify(transactionDetailService, times(1)).saveTransactionData(transactionDto);


	}
	
	
	@Test 
	public void getTransactionData() {
		when(transactionDetailService.saveTransactionData(transactionDto)).thenReturn(response);
		ResponseEntity<List<TransactionSummary>> expected = transactionDetailController.fetchTransactionDataOnBasisOfCustomerAccountNumber(Mockito.anyString());        
		assertEquals(HttpStatus.OK, expected.getStatusCode());
		Mockito.verify(transactionDetailService, times(1)).fetchTransactionDataOnBasisOfCustomerAccountNumber(Mockito.anyString()
				);


	}
}
