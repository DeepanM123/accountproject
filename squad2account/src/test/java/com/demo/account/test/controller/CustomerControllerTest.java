package com.demo.account.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.demo.account.controller.CustomerController;
import com.demo.account.dto.LoginDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.service.CustomerService;

@RunWith(org.mockito.junit.MockitoJUnitRunner.Silent.class)
public class CustomerControllerTest {

	
	public ResponseEntity<TransactionResponse> loginCustomer(@RequestBody LoginDto loginDto) {
        return new ResponseEntity<TransactionResponse>(customerService.login(loginDto), HttpStatus.OK);
    }
	
	@Mock
	CustomerService customerService;

	@InjectMocks
	CustomerController customerController;




	LoginDto loginDto = new LoginDto();




	TransactionResponse response = new TransactionResponse("Transaction Successfully", 200);


	
	@Before
	public void setup() {
		loginDto.setCustomerAccountNumber("123");
		loginDto.setPassword("123");
	}    
	
	@Test 
	public void loginCustomerTest() {
		when(customerService.login(loginDto)).thenReturn(response);
		ResponseEntity<TransactionResponse> expected = customerController.loginCustomer(loginDto);        
		assertEquals(HttpStatus.OK, expected.getStatusCode());
		Mockito.verify(customerService, times(1)).login(loginDto);

	}


	
}
