package com.demo.account.test.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.demo.account.dto.TransactionDto;
import com.demo.account.entity.TransactionSummary;
import com.demo.account.exception.ServiceException;
import com.demo.account.repository.BeneficiaryRepository;
import com.demo.account.repository.CustomerJpaRepository;
import com.demo.account.repository.TransactionJpaRepository;
import com.demo.account.serviceImpl.TransactionDetailServiceImpl;

@RunWith(org.mockito.junit.MockitoJUnitRunner.Silent.class)
public class SavingAccountServiceImplTest {
	
	
	@Mock
	TransactionJpaRepository transactionJpaRepository;
	
	@InjectMocks
	TransactionDetailServiceImpl transactionDetailServiceImpl;
	
	@Mock
	BeneficiaryRepository beneficiaryRepository;
	
	@Mock
	CustomerJpaRepository customerJpaRepository;
	
	@Before
	public void setUp() {
	
	}
	
	@After
	public void setUp1() {
		
	}
	
	@Test
	public void testSaveTransactionData() {
		TransactionSummary transactionSummary = new TransactionSummary();
		transactionSummary.setAmountTransferred(100);
		transactionSummary.setFromAccount("123");
		transactionSummary.setToAccount("321");
		
		TransactionDto td = new TransactionDto();
		td.setAmountTransferred(100);
		td.setFromAccount("123");
		td.setToAccount("321");
		
		Mockito.when(beneficiaryRepository.checkFromAccountAndToAccount(transactionSummary.getFromAccount(), transactionSummary.getToAccount())).thenReturn(1);
		Mockito.when(customerJpaRepository.getTotalSavingsOfCustomer("123")).thenReturn(1000000.0);
		
		Mockito.when(transactionJpaRepository.save(transactionSummary)).thenReturn(transactionSummary);
		transactionDetailServiceImpl.saveTransactionData(td);
		
	}
	
	
	@Test(expected=ServiceException.class)
	public void testSaveTransactionDataNegative() {
		TransactionSummary transactionSummary = new TransactionSummary();
		transactionSummary.setAmountTransferred(100);
		transactionSummary.setFromAccount("123");
		transactionSummary.setToAccount("321");
		
		TransactionDto td = new TransactionDto();
		td.setAmountTransferred(100);
		td.setFromAccount("123");
		td.setToAccount("321");
		
		Mockito.when(beneficiaryRepository.checkFromAccountAndToAccount(transactionSummary.getFromAccount(), transactionSummary.getToAccount())).thenReturn(1);
		Mockito.when(customerJpaRepository.getTotalSavingsOfCustomer(td.getFromAccount())).thenReturn(1.0);
		
		Mockito.when(transactionJpaRepository.save(transactionSummary)).thenReturn(transactionSummary);
		transactionDetailServiceImpl.saveTransactionData(td);
		Mockito.atLeastOnce();
	}
	
	@Test(expected=ServiceException.class)
	public void testSaveTransactionDataNegative2() {
		TransactionSummary transactionSummary = new TransactionSummary();
		transactionSummary.setAmountTransferred(100);
		transactionSummary.setFromAccount("13");
		transactionSummary.setToAccount("321");
		
		TransactionDto td = new TransactionDto();
		td.setAmountTransferred(100);
		td.setFromAccount("123");
		td.setToAccount("321");
		
		Mockito.when(beneficiaryRepository.checkFromAccountAndToAccount(transactionSummary.getFromAccount(), transactionSummary.getToAccount())).thenReturn(1);
		Mockito.when(customerJpaRepository.getTotalSavingsOfCustomer(td.getFromAccount())).thenReturn(1.0);
		
		Mockito.when(transactionJpaRepository.save(transactionSummary)).thenReturn(transactionSummary);
		transactionDetailServiceImpl.saveTransactionData(td);
		Mockito.atLeastOnce();
	}
	

	@Test
	public void testfetchTransactionDataOnBasisOfCustomerAccountNumber() {
		TransactionSummary ts = new TransactionSummary();
		ts.setAmountTransferred(10000);
		ts.setFromAccount("123");
		ts.setToAccount("321");
		List<TransactionSummary> ls = new ArrayList<>();
		ls.add(ts);
		Mockito.when(transactionJpaRepository.fetchData(ts.getFromAccount())).thenReturn(ls);
		transactionDetailServiceImpl.fetchTransactionDataOnBasisOfCustomerAccountNumber("123");
	}
	

	@Test(expected=ServiceException.class)
	public void testfetchTransactionDataOnBasisOfCustomerAccountNumberNegative() {
		TransactionSummary ts = new TransactionSummary();
		ts.setAmountTransferred(10000);
		ts.setFromAccount("123");
		ts.setToAccount("321");
		List<TransactionSummary> ls = new ArrayList<>();
		ls.add(ts);
		Mockito.when(transactionJpaRepository.fetchData(ts.getFromAccount())).thenThrow(ServiceException.class);
		transactionDetailServiceImpl.fetchTransactionDataOnBasisOfCustomerAccountNumber("1");
	}
}
