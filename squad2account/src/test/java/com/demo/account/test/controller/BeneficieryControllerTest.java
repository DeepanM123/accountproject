package com.demo.account.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.demo.account.controller.BeneficiaryController;
import com.demo.account.dto.BeneficiaryDto;
import com.demo.account.dto.TransactionResponse;
import com.demo.account.entity.Beneficiary;
import com.demo.account.exception.ServiceException;
import com.demo.account.service.BeneficiaryService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BeneficieryControllerTest {
	
	@Mock
	BeneficiaryService beneficiaryService;
	
	@InjectMocks
	BeneficiaryController beneficieryController;
	
	
	TransactionResponse tr = new TransactionResponse("Beneficiery added", 200);
	
	BeneficiaryDto beneficiaryDto = new BeneficiaryDto();
	 Beneficiary beneficiary = new Beneficiary();
	
	
	 List<Beneficiary> lb = new ArrayList<Beneficiary>();
	@Before
	public void setUp() {
		
		beneficiary.setBeneficiaryAccountNumber("321");
		beneficiary.setBeneficiaryName("Alok");
		beneficiary.setCustomerAccountNumber("123");
		
		lb.add(beneficiary);
		beneficiaryDto.setCustomerAccountNumber("123");
		beneficiaryDto.setListBeneficiary(lb);
	}
	
	@Test
	public void addBeneficiaryTest() {
		when(beneficiaryService.saveBeneficiaryData(beneficiaryDto)).thenReturn(tr);
		ResponseEntity<TransactionResponse> expected = beneficieryController.addBeneficiary(beneficiaryDto);
		assertEquals(HttpStatus.OK, expected.getStatusCode());
		Mockito.verify(beneficiaryService, times(1)).saveBeneficiaryData(beneficiaryDto);
	}
	


	@Test
	public void fetchBeneficiaryTest() {
		when(beneficiaryService.fetchBeneficiaryData("123")).thenReturn(lb);
		ResponseEntity<List<Beneficiary>> expected = beneficieryController.getBeneficiaryDetails("customerAccountNumber");
		assertEquals(HttpStatus.OK, expected.getStatusCode());
		Mockito.verify(beneficiaryService, times(1)).fetchBeneficiaryData(Mockito.anyString());
	}
	
	 @Test
	    public void BeneficiaryDetails1() throws ServiceException, ParseException {
	        Mockito.when(beneficiaryService.saveBeneficiaryData(Mockito.any(BeneficiaryDto.class)))
	                .thenReturn(tr);
	        ResponseEntity<TransactionResponse> response = beneficieryController.addBeneficiary(beneficiaryDto);
	        Assert.assertNotNull(response);
	        Assert.assertEquals(tr, response.getBody());
	    }

	 

	    @Test
	    public void BeneficiaryDetails() throws ServiceException, ParseException {
	        Mockito.when(beneficiaryService.fetchBeneficiaryData(Mockito.anyString())).thenReturn(lb);
	        ResponseEntity<List<Beneficiary>> response = beneficieryController.getBeneficiaryDetails(Mockito.anyString());
	        Assert.assertNotNull(response);
	       // Assert.assertEquals(lb.size(), ((List<Beneficiary>) response).size());
	    }

	 

	}


